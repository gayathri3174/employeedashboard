import React, { useState, useEffect } from 'react';
import './TableRepresentation.css';
import { useSelector } from 'react-redux';

const TableRepresentation = ({ columnname }) => {
  const data = useSelector((state) => state.selectedData);

  const getCountsByCountry = () => {
    const counts = {};
    data.forEach((item) => {
      const country = item.Country;
      counts[country] = counts[country] ? counts[country] + 1 : 1;
    });
    return Object.entries(counts).map(([country, count]) => ({ _id: country, count }));
  };

  const [countryCounts, setCountryCounts] = useState(getCountsByCountry());

  useEffect(() => {
    setCountryCounts(getCountsByCountry());
  }, [data]);

  return (
    <div className='m-2' style={{ border: '1px solid white' }}>
      <h1 style={{ fontSize: '1rem', fontWeight: 'bold', textAlign: 'center', color: '#0A6E7C' }}>{columnname}</h1>

      <div style={{ textAlign: 'center', borderCollapse: 'collapse', height: '300px', overflow: 'scroll' }}>
        <table>
          <thead>
            <tr>
              <th>{columnname}</th>
              <th>Count</th>
            </tr>
          </thead>
          <tbody>
            {countryCounts.map((country, index) => (
              <tr key={index} style={{ border: '1px solid black' }}>
                <td style={{ width: '200px' }}>{country._id}</td>
                <td>{country.count}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default TableRepresentation;
